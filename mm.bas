
#If __FB_DEBUG__ <> 0
	#Define DEBUG
#EndIf


#Include "crt.bi"
#Include "stwList.bi"
#Include "stwMap.bi"


Const FALSE = 0
Const TRUE = Not FALSE

#Define MAX(a,b) (IIf((a)>(b),(a),(b)))
#Define MIN(a,b) (IIf((a)<(b),(a),(b)))


'Type

Type BlockFwd As Block



' Type descriptor:
' 4 bytes: object size (includes 4 bytes for TypeTag; has to be a multiple of 4)
'      4 bytes: pointer to next TypeTag in free list
' OR n*4 bytes: list of pointer offsets; terminates with -1

Type TypeDesc Field=4
	sz As Integer
	Union
		entries (0 To 9) As Integer
		nx As BlockFwd Ptr
	End Union
	Declare Sub setPtrOffsets Cdecl (p As Integer, ...)
	Declare Property size(v As Integer)
	Declare Property size() As Integer
End Type



defineMap(String, TypeDesc Ptr, TypeDesc, 0,0)


'  32-bits:
' 1111 1111 0000 0000 1111 1111 0000 0000
' |<------ tag = type desc ptr ------>| ^mark
'                                      ^free
Type TypeTag Field=4
  Dim tag As UInteger
  Declare Property marked() As Integer
  Declare Property marked(v As Integer)
  Declare Property isFree() As Integer
  Declare Property isFree(v As Integer)
  Declare Property desc() As TypeDesc Ptr
  Declare Property desc(v As TypeDesc Ptr)
  Declare Property dat() As UInteger Ptr
End Type

Property TypeTag.marked() As Integer
	Return IIf(tag And 1, -1, 0)
End Property

Property TypeTag.marked(v As Integer)
	If v Then tag Or= 1 Else tag And= &HFFFFFFFE
End Property

Property TypeTag.isFree() As Integer
	Return IIf(tag And 2, -1, 0)
End Property

Property TypeTag.isFree(v As Integer)
	If v Then tag Or= 2	Else tag And= &HFFFFFFFD
End Property

Property TypeTag.desc() As TypeDesc Ptr
	AssertWarn(tag And &HFFFFFFFC)
	Return Cast(TypeDesc Ptr, tag And &HFFFFFFFC)
End Property

Property TypeTag.desc(v As TypeDesc Ptr)
	tag = (tag And 3) Or (Cast(UInteger, v) And &HFFFFFFFC)
End Property

Property TypeTag.dat() As UInteger Ptr
	Return Cast(UInteger Ptr, @This)+1
End Property



Property TypeDesc.size(v As Integer)
	'ensure minimal size of 12 bytes so that it can be converted to a free block
	this.sz = MAX(v + SizeOf(TypeTag), 12)
End Property

Property TypeDesc.size() As Integer
	Return this.sz - SizeOf(TypeTag)
End Property


Sub TypeDesc.setPtrOffsets Cdecl (count As Integer, ... )
    Dim arg As Any Ptr = va_first()
    For q As Integer = 1 To count
        this.entries(q-1) = va_arg(arg, Integer)
        arg = va_next(arg, Integer)
    Next
    entries(count) = -count*SizeOf(TypeTag) - SizeOf(TypeTag)
End Sub




Type Block
	dummy As Integer
	Declare Property tag() As TypeTag Ptr
	Declare Property length() As Integer
	Declare Property length(v As Integer)
	Declare Property nextFree() As Block Ptr
	Declare Property nextFree(v As Block Ptr)
	Declare Property nextBlock() As Block Ptr
	Declare Sub setFree(l As Integer, n As Block Ptr)
End Type


Property Block.tag() As TypeTag Ptr
	Return Cast(TypeTag Ptr, Cast(UInteger Ptr, @This)-1)
End Property

Property Block.length() As Integer
	Return this.tag->desc->sz
End Property

Property Block.length(v As Integer)
#Ifdef DEBUG
	If Cast(Integer, this.tag->desc) <> Cast(Integer, this.tag)+SizeOf(TypeTag) Then Print "Warning: modifying type size!"
#EndIf
	this.tag->desc->sz = v
End Property

Sub Block.setFree(l As Integer, n As Block Ptr)
#Ifdef DEBUG
	Print "Debug: Freed block "; n; " with length "; l
#EndIf
	this.tag->desc = Cast(TypeDesc Ptr, @This)
	this.tag->marked = 0
	this.tag->isFree = 1
	this.tag->desc->sz = l
	this.tag->desc->nx = n
End Sub

Property Block.nextFree() As Block Ptr
#Ifdef DEBUG
	If this.tag->isFree = 0 OrElse Cast(Integer, this.tag->desc) <> Cast(Integer, this.tag)+SizeOf(TypeTag) Then Print "Warning: accessing next block field in non-free block!"
#EndIf
	Return this.tag->desc->nx
End Property

Property Block.nextFree(v As Block Ptr)
#Ifdef DEBUG
	If this.tag->isFree = 0 OrElse Cast(Integer, this.tag->desc) <> Cast(Integer, this.tag)+SizeOf(TypeTag) Then Print "Warning: changing next block field in non-free block!"
#EndIf
	this.tag->desc->nx = v
End Property

Property Block.nextBlock() As Block Ptr
	Return Cast(Block Ptr, Cast(UInteger, @This) + this.length)
End Property



#Ifdef DEBUG

defineList(Block Ptr, BlockPtr, 0)
Dim Shared allocList As stwListBlockPtr

#EndIf



Type Heap

Private:
	Dim heap As Any Ptr
	Dim heapSz As UInteger
	Dim types As stwMapTypeDesc
	Dim ffree As Block Ptr
	
	Declare Property firstBlock() As Block Ptr
	Declare Property maxBlock() As Any Ptr
	Declare Function findPrevFree(b As Block Ptr) As Block Ptr
	Declare Function findPrevBlock(b As Block Ptr) As Block Ptr
	
	Declare Function mark(cur As Block Ptr) As Integer
	Declare Function sweep() As Integer
	
Public:
	Declare Constructor()
	Declare Destructor()
	
	
	Declare Function alloc(typ As String) As Any Ptr
	Declare Function dealloc(b As Any Ptr) As Integer
	Declare Function register(typ As String, desc As TypeDesc Ptr) As Integer
	Declare Function gc(roots As Block Ptr Ptr) As Integer
	Declare Function dump() As Integer
	Declare Function dumpToFile(filename As String) As Integer
	
End Type


Constructor Heap()
	'heap of size 32 Kbytes
	this.heapSz = 32*1024
	this.heap = Allocate(heapSz)
#Ifdef DEBUG
	memset(this.heap, &hCC, this.heapSz)
#EndIf
	ffree = this.firstBlock
	ffree->setFree(heapSz, 0)
End Constructor

Destructor Heap()
	DeAllocate(this.heap)
	this.heap = 0
End Destructor

Property Heap.firstBlock() As Block Ptr
	Return this.heap+SizeOf(TypeTag)
End Property

Property Heap.maxBlock() As Any Ptr
	Return Cast(Any Ptr, Cast(UInteger, this.heap) + this.heapSz)
End Property
	
Function Heap.findPrevFree(b As Block Ptr) As Block Ptr
	Dim prevFree As Block Ptr = 0
	Dim free As Block Ptr = ffree
	While free < b AndAlso free <> 0
		prevFree = free
		free = free->nextFree
	Wend
	Return prevFree
End Function


Function Heap.findPrevBlock(b As Block Ptr) As Block Ptr
	Dim prev As Block Ptr = 0
	Dim cur As Block Ptr = this.firstBlock
	While cur < b AndAlso cur < this.maxBlock
		prev = cur
		cur = cur->nextBlock
	Wend
	Return prev
End Function


'' see slide 01/12
Function Heap.alloc(typ As String) As Any Ptr
	' This function should allocate an object with the required size,
	' install in it a pointer to the type descriptor of class Student,
	' and return the address of the new object.
	
#Ifdef DEBUG
	Print "Debug: allocate '"; typ; "' object"
#EndIf
	
	'check whether type is known
	If this.types.Contains(typ) = 0 Then
		Print "Error: cannot allocate object of type '"; typ; "' - type is unknown"
		Return 0
	EndIf
	Dim td As TypeDesc Ptr = this.types[typ]
	
	' First Fit: returns the first suitable block with length >= size + sizeof(TypeTag)
	Dim start As Block Ptr = ffree
	Dim prev As Block Ptr = ffree
	Dim free As Block Ptr = ffree
	While free->length < td->sz+12 AndAlso free <> 0	'assure that block can be split (12 bytes remain)
		prev = free
		free = free->nextFree
	Wend
	If free = 0 Then
		Print "Error: out of memory - unable to allocate "; td->sz; " bytes for type "; typ
		Return 0
	EndIf
	Dim p As Block Ptr = free
	Dim newLen As Integer = p->length - td->sz
	If newLen > 8 Then
		'split block
		p = Cast(Block Ptr, Cast(Integer, p)+p->length-td->sz)
		free->length = newLen
	ElseIf free = prev Then
		'do not allow this case to fix freeBlock > allocatedBlock issue
		Assert(0)
		'last free block
		ffree = 0
	Else
		'do not allow this case to fix freeBlock > allocatedBlock issue
		Assert(0)
		'remove block from list
		prev->nextFree = free->nextFree
		ffree = prev
	EndIf
	memset(p, 0, td->sz-SizeOf(TypeTag))
	p->tag->desc = td
	p->tag->isFree = 0
	p->tag->marked = 0
#Ifdef DEBUG
	allocList.Add(p)
#EndIf
	
	Return p
End Function


''see slide 01/13
Function Heap.dealloc(p As Any Ptr) As Integer
	Dim b As Block Ptr = p
	AssertWarn(b >= this.firstBlock)
	AssertWarn(b < this.maxBlock)
	If b->tag->isFree <> 0 Then
		Print "Error: trying to deallocate free memory block "; b
		Return 0
	EndIf
	If b->tag->desc = 0 Then
		Print "Warning: trying to deallocate a possibly invalid block"; b
	EndIf
#Ifdef DEBUG
	Print "Debug: deallocate block "; b
#EndIf
#Ifdef DEBUG
	allocList.Remove(p)
#EndIf
	b->tag->isFree = 1	'mark as free for possibly detecting duplicate deallocations
	Dim leftN As Block Ptr = this.findPrevBlock(b)
	Dim rightN As Block Ptr = b->nextBlock
	Dim leftF As Block Ptr = this.findPrevFree(b)
	If leftN <> 0 AndAlso leftN->tag->isFree Then
		'merge left and p; p=left
		leftN->length = leftN->length + b->length
		b = leftN
	Else
		'add p to free list
		If leftF = 0 Then
			b->setFree(b->length, ffree)
			ffree = b
		Else
			'FIXME: setFree may write into unallocated memory as writes 12 bytes while a allocated block may be only 8 bytes
			b->setFree(b->length, leftF->nextFree)
			leftF->nextFree = b
		EndIf
	EndIf
	If rightN < this.maxBlock AndAlso rightN->tag->isFree Then
		'remove right from the free list (find previous free block)
		'merge p and right
		'leftF->nextFree = b
		b->setFree(b->length+rightN->length, rightN->nextFree)
	EndIf
	Return 0
End Function



Function Heap.register(typ As String, desc As TypeDesc Ptr) As Integer
	this.types.Add(typ, desc)
	Return -1
End Function




Function Heap.dump() As Integer
	Dim totFree As UInteger = 0
	Dim b As Block Ptr = this.firstBlock
	Print "=== DUMP BEGINS ==="
	Print "Live objects:"
	While b < this.maxBlock
		If b->tag->isFree=0 Then
			Print "(addr: "; Hex(Cast(UInteger, b), 8); ", name:"; types.Lookup(b->tag->desc); ", first 4 bytes: "; Hex(*CPtr(UInteger Ptr, b),8); ")"
			For q As Integer = 0 To 9 'UBound(b->tag->desc->entries)
				If b->tag->desc->entries(q) < 0 Then Exit For
				Print " - Pointer at "; b->tag->desc->entries(q); " = "; Hex(*CPtr(UInteger Ptr, CPtr(Byte Ptr, b)+b->tag->desc->entries(q)),8)
			Next
		EndIf
		b = b->nextBlock
	Wend

	b = this.firstBlock
	Print "Free blocks:"
	While b < this.maxBlock
		If b->tag->isFree Then
			Print "(addr: "; Hex(Cast(UInteger, b), 8); ", length:"; b->length; ")"
			totFree += b->length				'TODO: subtract size for tag?
		EndIf
		b = b->nextBlock
	Wend
	Print "Total Free: "; totFree
	Print "=== DUMP ENDS ==="
	Return 0
End Function



Function Heap.dumpToFile(filename As String) As Integer
#Ifdef DEBUG
	Dim ff As Integer = FreeFile
	Open filename For Binary As #ff
	Put #ff, 0, *Cast(UByte Ptr, this.heap), this.heapSz
	Close #ff
#EndIf
	Return 0
End Function



/'
Deutsch-Schorr-Waite:

advance:
p = cur.son[cur.i];
cur.son[cur.i] = prev;
prev = cur;
cur = p;

retreat:
p = cur;
cur = prev;
prev = cur.son[cur.i];
cur.son[cur.i] = p;

'/


'' see slide 02/24
Function Heap.mark(cur As Block Ptr) As Integer
	Assert(cur <> 0)
	Assert(cur->tag->marked = 0)
	
	Dim prev As Block Ptr = 0
	Dim off As Integer
	Dim pAdr As Block Ptr Ptr
	Dim p As Block Ptr
	
#Ifdef DEBUG
	Print "Debug: marking root node: "; types.Lookup(cur->tag->desc)
#EndIf
	cur->tag->marked = 1
	Do
		Dim a As block Ptr
		'i++ (pointer to son[i])
		cur->tag->desc = Cast(TypeDesc Ptr, Cast(Integer, cur->tag->desc) + SizeOf(Any Ptr))
		'offset of son[i]
		off = *Cast(Integer Ptr, cur->tag->desc)
		If off >= 0 Then
			'advance
			'address of son[i] = baseAddr + offset
			pAdr = Cast(Block Ptr Ptr, Cast(Integer, cur) + off)
			'pointer val at son[i]
			p = *pAdr
			'if pointer valid and not yet marked then
			If p <> 0 AndAlso p->tag->marked = 0 Then
				'save prev at that pointer slot
				*pAdr = prev
				'update prev to cur
				prev = cur
				'.. and cur to p (pointer val)
				cur = p
#Ifdef DEBUG
				Print "Debug: marking node: "; types.Lookup(cur->tag->desc)
#EndIf
				cur->tag->marked = 1
			EndIf
		Else
			'retreat
			'restore tag: offset = -count*4 - 4
			cur->tag->desc = Cast(TypeDesc Ptr, Cast(Integer, cur->tag->desc) + off)		'restore tag
			'iterated over whole tree -> return
			If prev = 0 Then Return 0
			'save current
			p = cur
			'set back cur to previous
			cur = prev
			'
			off = *Cast(Integer Ptr, cur->tag->desc)
			pAdr = Cast(Block Ptr Ptr, Cast(Integer, cur) + off)
			prev = *pAdr 
			'restore pointer
			*pAdr = p
		EndIf
	Loop
	
End Function


'' see slide 02/26
Function Heap.sweep() As Integer
	Dim p As Block Ptr = this.firstBlock
	this.ffree = 0
	Dim lastFree As Block Ptr = 0
	While p < this.maxBlock
		If p->tag->marked Then
			p->tag->marked = 0
		Else
			'free: collect p
#Ifdef DEBUG
			If p->tag->isFree Then
				Print "Debug: collecting free block "; p
			Else
				Print "Debug: collecting unreferenced block "; p; " ("; types.Lookup(p->tag->desc); ")"
				allocList.Remove(p)
			EndIf
#EndIf
			Dim size As Integer = p->length
			Dim q As Block Ptr = p->nextBlock
			While q < this.maxBlock AndAlso q->tag->marked = 0
#Ifdef DEBUG
				If q->tag->isFree Then
					Print "Debug: collecting sequential free block "; q
				Else
					Print "Debug: collecting sequential unreferenced block "; q; " ("; types.Lookup(q->tag->desc); ")"
					allocList.Remove(q)
				EndIf
#EndIf
				size += q->length
				q = q->nextBlock
			Wend
			'use slightly more complex add logic to get ascending addresses in the free list
			p->setFree(size, 0)
			If this.ffree = 0 Then 
				this.ffree = p 
				Else
				 lastFree->nextFree = p
			EndIf
			lastFree = p
			'p->setFree(size, free)
			'free = p

		EndIf
		p = p->nextBlock
	Wend
	
	Return 0
End Function



Function Heap.gc(roots As Block Ptr Ptr) As Integer
	Assert(roots <> 0)
	Dim p As Block Ptr Ptr = roots
	While *p <> 0
		this.mark(*p)
		p+=1
	Wend
	this.sweep()
	Return 0
End Function




''==================================================================================================
''==================================================================================================
''===================================    TEST  PROGRAM    ==========================================
''==================================================================================================
''==================================================================================================

Dim Shared h As Heap

Dim rootPtr(0 To 1) As Block Ptr


h.dumpToFile("00_initial.dmp")


Type Lecture
	id As Integer
	nam As String*50
	sem As Integer
End Type

Type LectureNode
	nx As LectureNode Ptr
	lect As Lecture Ptr
End Type


Type Student
	id As Integer
	nam As String*50
	lect As LectureNode Ptr
	Declare Sub Add(l As Lecture Ptr)
	Declare Sub Remove(l As Lecture Ptr)
End Type

Sub Student.Add(l As Lecture Ptr)
	Dim n As LectureNode Ptr = CPtr(LectureNode Ptr, h.alloc("LectureNode"))
	n->lect = l
	n->nx = this.lect
	this.lect = n
End Sub

Sub Student.Remove(l As Lecture Ptr)
	Dim n As LectureNode Ptr = this.lect
	If n<>0 AndAlso n->lect = l Then this.lect = n->nx: Return
	While n<>0
		If n->nx<> 0 AndAlso n->nx->lect = l Then n->nx = n->nx->nx: Return
		n = n->nx
	Wend
End Sub


Type StudentNode
	nx As StudentNode Ptr
	stud As Student Ptr
End Type


Type StudentList
	first As StudentNode Ptr
	Declare Sub Add(s As Student Ptr)
	Declare Sub Remove(s As Student Ptr)
End Type


Sub StudentList.Add(s As Student Ptr)
	Dim n As StudentNode Ptr = CPtr(StudentNode Ptr, h.alloc("StudentNode"))
	n->stud = s
	n->nx = this.first
	this.first = n
End Sub

Sub StudentList.Remove(s As Student Ptr)
	Dim n As StudentNode Ptr = this.first
	If n<>0 AndAlso n->stud = s Then this.first = n->nx: Return
	While n<>0
		If n->nx<> 0 AndAlso n->nx->stud = s Then n->nx = n->nx->nx: Return
		n = n->nx
	Wend
End Sub



'Register Types
Dim As TypeDesc tLecture, tLectureNode, tStudent, tStudentNode, tStudentList

tLecture.size = SizeOf(Lecture)
tLecture.setPtrOffsets(0)
tLectureNode.size = SizeOf(LectureNode)
tLectureNode.setPtrOffsets(2, OffsetOf(LectureNode, nx), OffsetOf(LectureNode, lect))
tStudent.size = SizeOf(Student)
tStudent.setPtrOffsets(1, OffsetOf(Student, lect))
tStudentNode.size = SizeOf(StudentNode)
tStudentNode.setPtrOffsets(2, OffsetOf(StudentNode, nx), OffsetOf(StudentNode, stud))
tStudentList.size = SizeOf(StudentList)
tStudentList.setPtrOffsets(1, OffsetOf(StudentList, first))

h.register("Lecture", @tLecture)
h.register("LectureNode", @tLectureNode)
h.register("Student", @tStudent)
h.register("StudentNode", @tStudentNode)
h.register("StudentList", @tStudentList)


Print "== "; tStudentList.sz

'Set test data
Dim list As StudentList Ptr
Dim As Student Ptr s1, s2, s3
Dim As Lecture Ptr l1, l2, l3

'h.dump()
'Sleep


list = CPtr(StudentList Ptr, h.alloc("StudentList"))
'h.dump()
'sleep

'set root Pointers
rootPtr(0) = CPtr(Block Ptr, list)
rootPtr(1) = 0

s1 = CPtr(Student Ptr, h.alloc("Student"))
s2 = CPtr(Student Ptr, h.alloc("Student"))
s3 = CPtr(Student Ptr, h.alloc("Student"))
s1->id=1: s1->nam="Hans M�ller"
s2->id=2: s2->nam="Peter Maier"
s3->id=3: s3->nam="Klaus Kleber"

l1 = CPtr(Lecture Ptr, h.alloc("Lecture"))
l2 = CPtr(Lecture Ptr, h.alloc("Lecture"))
l3 = CPtr(Lecture Ptr, h.alloc("Lecture"))
l1->id=1: l1->nam="System Software": l1->sem=7
l2->id=2: l2->nam="Algebra": l2->sem=2
l3->id=3: l3->nam="Formal Models": l3->sem=5

list->Add(s1)
list->Add(s2)
list->Add(s3)

s1->Add(l1)
s1->Add(l2)
s1->Add(l3)
s2->Add(l1)
s2->Add(l3)
s3->Add(l2)
s3->Add(l3)


h.dump()
'Sleep


h.dumpToFile("01_beforeGC.dmp")
h.gc(@rootPtr(0))
h.dumpToFile("02_afterGC.dmp")

/'
#Ifdef DEBUG
Print "# deallocations: "; allocList.Count
While allocList.Count > 0
	h.dealloc(allocList[0])
Wend
#EndIf
'/

/'
h.dealloc(s1->lect->nx->nx)
h.dealloc(s1->lect->nx)
h.dealloc(s1->lect)
h.dealloc(s2->lect->nx)
h.dealloc(s2->lect)
h.dealloc(s3->lect->nx)
h.dealloc(s3->lect)
h.dealloc(list->first->nx->nx)
h.dealloc(list->first->nx)
h.dealloc(list->first)
h.dealloc(l3)
h.dealloc(l2)
h.dealloc(l1)
h.dealloc(s1)
h.dealloc(s3)
h.dealloc(s2)
h.dealloc(list)
'h.dealloc(l3)			'duplicate deallocation!!
'h.dealloc(l3)			'duplicate deallocation!!
'/


h.dump()
'Sleep

'remove some elements (Student 1) -> referenced nodes should be deallocated in gc run
list->Remove(s1)

h.gc(@rootPtr(0))
h.dumpToFile("03_after2ndGC.dmp")


h.dump()
'Sleep


'delete root pointer => no elements referenced anymore
rootPtr(0) = 0
h.dumpToFile("05_beforeFinalGC.dmp")
h.gc(@rootPtr(0))


h.dump()
'Sleep


#Ifdef DEBUG
Print "# blocks allocated: "; allocList.Count
For q As Integer = 0 To allocList.Count-1
	Print "  : "; allocList[q]
Next
#EndIf

h.dumpToFile("06_end.dmp")


Sleep
















