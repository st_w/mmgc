
#Macro declareMap(OBJTYPE1, OBJTYPE2, MAPNAME)

#Ifndef stwMap##MAPNAME

#Include Once "crt.bi"

'ByRef return values supported in fbc >= 0.90.0
#If __FB_MIN_VERSION__(0,90,0)
#Define BYREFRETURN ByRef
#Else
'Return ByVal instead for older versions

#Define BYREFRETURN 
#EndIf

Type stwMap##MAPNAME
	
Private:
	_count As Integer
	_capacity As Integer
	_data As UByte Ptr
#Ifdef THREADSAFE
	_mutex As Any Ptr
#EndIf
	
Public:
	Declare Property Capacity() As Integer
	Declare Property Count() As Integer
	Declare Property ItemVal(index As Integer) BYREFRETURN As OBJTYPE2
	Declare Property ItemVal(index As Integer, value As OBJTYPE2)
	Declare Property ItemIdx(index As Integer) BYREFRETURN As OBJTYPE1
	Declare Property Item(index As OBJTYPE1) BYREFRETURN As OBJTYPE2
	Declare Property Item(index As OBJTYPE1, value As OBJTYPE2)
	Declare Property Lookup(index As OBJTYPE2) BYREFRETURN As OBJTYPE1
	Declare Sub Add(index As OBJTYPE1, value As OBJTYPE2)
	'Declare Sub AddRange(list As stwMap##MAPNAME)
	Declare Sub Clear_()
	Declare Function Contains(value As OBJTYPE1) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE1) As Integer
	Declare Function IndexOfRev Overload (value As OBJTYPE2) As Integer
	Declare Sub Remove (value As OBJTYPE1)
	Declare Sub RemoveAt (index As Integer)
	Declare Function ToString() As String
	Declare Sub TrimExcess()
	
	Declare Constructor ()
	Declare Constructor (initialCapacity As Integer)
	Declare Constructor (ByRef list As stwMap##MAPNAME)
	'TODO: List<T>(IEnumerable<T>)
	'Declare Constructor ()
	
	Declare Destructor()
	
	Declare Operator Let(list As stwMap##MAPNAME)
	
	#If __FB_MIN_VERSION__(0,91,0)
	Declare Operator [] (index As OBJTYPE1) ByRef As OBJTYPE2
	#EndIf
	
End Type

#EndIf

#EndMacro


#Macro implementMap(OBJTYPE1, OBJTYPE2, MAPNAME, CALL_DESTRUCTOR1, CALL_DESTRUCTOR2)

'Initializes a new instance of the List<T> class that is empty and has the default initial capacity.
Constructor stwMap##MAPNAME()
	this._data = Callocate(16 * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	this._capacity = 16
	this._count = 0
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#endif
End Constructor

'Initializes a new instance of the List<T> class that is empty and has the specified initial capacity.
Constructor stwMap##MAPNAME(initialCapacity As Integer)
	this._data = Callocate(initialCapacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	this._capacity = initialCapacity
	this._count = 0
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#EndIf
End Constructor

'Copy Constructor
Constructor stwMap##MAPNAME(ByRef list As stwMap##MAPNAME)
	#If __FB_DEBUG__
		'Print "stwMap: COPY CONSTRUCTOR was called"
	#EndIf
	this._data = Callocate(list._capacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	memcpy(this._data, list._data, list._capacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	this._capacity = list._capacity
	this._count = list._count
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#EndIf
End Constructor


Destructor stwMap##MAPNAME()
	#If TypeOf(OBJTYPE1) = "STRING"
	this.Clear_()
	#ElseIf CALL_DESTRUCTOR1
	this.Clear_()
	#ElseIf TypeOf(OBJTYPE2) = "STRING"
	this.Clear_()
	#ElseIf CALL_DESTRUCTOR2
	this.Clear_()
	#EndIf
	DeAllocate(this._data)
#Ifdef THREADSAFE
	MutexDestroy(this._mutex)
#EndIf
End Destructor


'Gets or sets the total number of elements the internal data structure can hold without resizing.
'TODO: setter
Property stwMap##MAPNAME.Capacity() As Integer
	Return this._capacity
End Property

'Gets the number of elements contained in the List<T>.
Property stwMap##MAPNAME.Count() As Integer
	Return this._count
End Property





'Gets the element-index at the specified index.
Property stwMap##MAPNAME.ItemIdx(index As Integer) BYREFRETURN As OBJTYPE1
	#If __FB_DEBUG__
		'Print "INFO: Accessing element at index "; index; " value: "; str(*Cast(OBJTYPE ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))))
		If index < 0 Or index >= this._count Then Print "ERROR: list.ItemIdx(index) read access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
'FIXME
	'If index < 0 Or index >= this._count Then Return 0
	Return *Cast(OBJTYPE1 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
End Property

'Gets or sets the element-value at the specified index.
Property stwMap##MAPNAME.ItemVal(index As Integer) BYREFRETURN As OBJTYPE2
	#If __FB_DEBUG__
		'Print "INFO: Accessing element at index "; index; " value: "; str(*Cast(OBJTYPE ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))))
		If index < 0 Or index >= this._count Then Print "ERROR: list.ItemVal(index) read access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
'FIXME
	'If index < 0 Or index >= this._count Then Return 0
	Return *Cast(OBJTYPE2 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1))
End Property

Property stwMap##MAPNAME.ItemVal(index As Integer, value As OBJTYPE2)
	#If __FB_DEBUG__
		If index < 0 Or index >= this._count Then Print "ERROR: list.ItemVal(index) write access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
	If index < 0 Or index >= this._count Then Return
	*Cast(OBJTYPE2 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1)) = value
End Property


'Gets or sets the element-value at the specified element-index.
Property stwMap##MAPNAME.Item(idx As OBJTYPE1) BYREFRETURN As OBJTYPE2
	Return ItemVal(IndexOf(idx))
End Property

Property stwMap##MAPNAME.Item(idx As OBJTYPE1, value As OBJTYPE2)
	ItemVal(IndexOf(idx)) = value
End Property

'Gets or sets the element-value at the specified element-index.
Property stwMap##MAPNAME.Lookup(idx As OBJTYPE2) BYREFRETURN As OBJTYPE1
	Return ItemIdx(IndexOfRev(idx))
End Property



'Adds an object to the end of the List<T>.
Sub stwMap##MAPNAME.Add(index As OBJTYPE1, value As OBJTYPE2)
	If this.Contains(index) Then Return
	If this._count >= this._capacity Then
		Dim As Integer newCapacity = this._capacity + Int(this._capacity / 2)
		'Print "INFO: Reallocating data array; old-cap: "; this._capacity; ", new-cap: "; newCapacity
		this._data = ReAllocate(this._data, newCapacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
		memset(this._data + this._capacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)), 0, (newCapacity-this._capacity)* (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))) 		'FIXME _data = 0
		this._capacity = newCapacity
	EndIf
	#If __FB_DEBUG__
		'Print "INFO: Adding element at index "; this._count
		If this._data = 0 Then Print "ERROR: list.Add(value) cannot allocate memory: capacity: "; this._capacity
	#EndIf
	If this._data = 0 Then Return 'FIXME
	#If __FB_DEBUG__
		For q As Integer = 0 To (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))-1
			If *(this._data + this._count * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + q) <> 0 Then Print "WARNING: Add(value): accessing uninitialized memory, count: "; this._count
		Next
	#EndIf
	*Cast(OBJTYPE1 ptr, this._data + this._count * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))) = index
	*Cast(OBJTYPE2 ptr, this._data + this._count * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1)) = value
	this._count += 1
End Sub

'Removes all elements from the List<T>.
Sub stwMap##MAPNAME.Clear_()
	#If TypeOf(OBJTYPE1) = "STRING"
	For q As Integer = 0 To this._count-1
		fb_StrDelete(*Cast(OBJTYPE1 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))))
	Next
	#EndIf
	#If CALL_DESTRUCTOR1
		For q As Integer = 0 To this._count-1
			(*Cast(OBJTYPE1 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))).Destructor()
		Next
	#EndIf
	#If TypeOf(OBJTYPE2) = "STRING"
	For q As Integer = 0 To this._count-1
		fb_StrDelete(*Cast(OBJTYPE2 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1)))
	Next
	#EndIf
	#If CALL_DESTRUCTOR2
		For q As Integer = 0 To this._count-1
			(*Cast(OBJTYPE2 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1))).Destructor()
		Next
	#EndIf
	#If (TypeOf(OBJTYPE1) = "STRING") OrElse (TypeOf(OBJTYPE2) = "STRING") OrElse (CALL_DESTRUCTOR1) OrElse (CALL_DESTRUCTOR2)
	memset(this._data, 0, this._count * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	#EndIf
	this._count = 0
End Sub

'Determines whether an element is in the List<T>.
Function stwMap##MAPNAME.Contains(index As OBJTYPE1) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE1 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))) = index Then Return -1
	Next
	Return 0
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the entire List<T>.
Function stwMap##MAPNAME.IndexOf(index As OBJTYPE1) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE1 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))) = index Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the entire List<T>.
Function stwMap##MAPNAME.IndexOfRev(index As OBJTYPE2) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE2 ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1)) = index Then Return q
	Next
	Return -1
End Function

'Removes the first occurrence of a specific object from the List<T>.
Sub stwMap##MAPNAME.Remove (index As OBJTYPE1)
	RemoveAt(IndexOf(index))
End Sub

'Removes the element at the specified index of the List<T>.
Sub stwMap##MAPNAME.RemoveAt (index As Integer)
	#If __FB_DEBUG__
		If index < 0 Or index >= this._count Then Print "ERROR: list.RemoveAt(index) write access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
	If index < 0 Or index >= this._count Then Return
	#If TypeOf(OBJTYPE1) = "STRING"
	'also initializes string descriptor to 0
	fb_StrDelete(*Cast(OBJTYPE1 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))))
	#Else
		#If CALL_DESTRUCTOR1
		(*Cast(OBJTYPE1 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))).Destructor()
		#EndIf
	#EndIf
	#If TypeOf(OBJTYPE2) = "STRING"
	'also initializes string descriptor to 0
	fb_StrDelete(*Cast(OBJTYPE2 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1)))
	#Else
		#If CALL_DESTRUCTOR2
		(*Cast(OBJTYPE2 ptr, this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)) + SizeOf(OBJTYPE1))).Destructor()
		#EndIf
	#EndIf
	memmove(this._data + index * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)), this._data + (index+1) * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)), (this._count-index-1) * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	this._count-=1
	memset(this._data + this._count * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)), 0, (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))	'set now unused element after last element to 0
End Sub


'Returns a string that represents the current object.
Function stwMap##MAPNAME.ToString() As String
	Dim As String s = ""
	'If this._count = 0 Then Print "<empty>"
	'For q As Integer = 0 To this._count-1
	'	s += Str(*Cast(OBJTYPE ptr, this._data + q * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2))))
	'	If q < this._count-1 Then s += ", "
	'Next
	Return s
End Function


'Sets the capacity to the actual number of elements in the List<T>, if that number is less than a threshold value.
Sub stwMap##MAPNAME.TrimExcess()
	If this._count > this._capacity*0.9 Then Return
	If this._count = 0 Then
		DeAllocate(this._data)
		this._data = Callocate(16 * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
		this._capacity = 16
	Else
		Dim As Integer newCapacity = this._count + Int(this._count/2)
		If newCapacity < 16 Then newCapacity = 16
		If this._capacity <= newCapacity Then Return
		this._data = ReAllocate(this._data, newCapacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
		this._capacity = newCapacity
	EndIf
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.TrimExcess() cannot allocate memory: capacity: "; this._capacity
	#EndIf
	'If this._data = 0 Then 'FIXME
End Sub

Operator stwMap##MAPNAME.Let(list As stwMap##MAPNAME)
	#If __FB_DEBUG__
		'Print "stwMap: LET OPERATOR was called"
	#EndIf
	this._data = Callocate(list._capacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	memcpy(this._data, list._data, list._capacity * (SizeOf(OBJTYPE1)+SizeOf(OBJTYPE2)))
	this._capacity = list._capacity
	this._count = list._count
	#Ifdef THREADSAFE
		this._mutex = MutexCreate()
	#EndIf
End Operator


#If __FB_MIN_VERSION__(0,91,0)

Operator stwMap##MAPNAME.[] (index As OBJTYPE1) ByRef As OBJTYPE2
	Return this.Item(index)
End Operator

#EndIf

#EndMacro




#Macro defineMap(OBJTYPE1, OBJTYPE2, MAPNAME, CALL_DESTRUCTOR1, CALL_DESTRUCTOR2)

declareMap(OBJTYPE1, OBJTYPE2, MAPNAME)
implementMap(OBJTYPE1, OBJTYPE2, MAPNAME, CALL_DESTRUCTOR1, CALL_DESTRUCTOR2)

#EndMacro
